import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

class RandomWordWidget extends StatefulWidget {
  const RandomWordWidget({Key? key}) : super(key: key);
  _RandomWordState createState() => _RandomWordState();
}

class _RandomWordState extends State<RandomWordWidget> {
  final _suggestions = <WordPair>[];
  final _saved = <WordPair>{};
  final _biggerFont = const TextStyle(fontSize: 18);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Startup name generator'),
        actions: [
          IconButton(icon: Icon(Icons.list), onPressed: _pushSaved, tooltip: 'Saved suggestions'),
        ],
      ),
      body: ListView.builder(
          padding: const EdgeInsets.all(16.0),
          itemBuilder: (context, i) {
            if (i.isOdd) return const Divider();

            final index = i ~/ 2;
            if (index >= _suggestions.length) {
              _suggestions.addAll(generateWordPairs().take(10));
            }
            final alreadySaved = _saved.contains(_suggestions[index]);
            return ListTile(
                title: Text(_suggestions[index].asPascalCase, style: _biggerFont),
                trailing: Icon(
                  alreadySaved ? Icons.favorite : Icons.favorite_border,
                  color: alreadySaved ? Colors.red : null,
                  semanticLabel: alreadySaved ? 'Remove from saved' : 'Save',
                ),
                onTap: () {
                  setState(() {
                    alreadySaved ? _saved.remove(_suggestions[index]) : _saved.add(_suggestions[index]);
                  });
                });
          }),
    );
  }

  manageSavedItems(bool alreadySaved, int index) {
    setState(() {
      alreadySaved ? _saved.remove(_suggestions[index]) : _saved.add(_suggestions[index]);
    });
  }

  _pushSaved() {
    Navigator.of(context).push(MaterialPageRoute<void>(builder: (context) {
      final tiles = _saved.map(
        (savedValue) {
          return ListTile(
            title: Text(
              savedValue.asPascalCase,
              style: _biggerFont,
            ),
          );
        },
      );
      final divided = tiles.isNotEmpty ? ListTile.divideTiles(context: context, tiles: tiles).toList() : <Widget>[];

      return Scaffold(
        appBar: AppBar(title: const Text('Saved suggestions')),
        body: ListView(children: divided),
      );
    }));
  }
}
