import 'package:flutter/material.dart';
import 'screens/random_words.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      // Application name
      title: 'Flutter Hello World',
      //theme: ThemeData(appBarTheme: AppBarTheme(backgroundColor: Colors.white, foregroundColor: Colors.black)),
      // A widget which will be started on application startup
      home: RandomWordWidget(),
    );
  }
}
